### Groupe - Jonathan, Marc, Amin, Hicham


# Requis
* WampServer 3.2.3
* Composer 2.0.8
* Symfony 5.4.6

# Installation

* Start WampServer
* `composer install`
* `php bin/console doctrine:database:create`
* `php bin/console make:migration`
* `php bin/console doctrine:migrations:migrate`
* Insert in database the posts with a request in posts.sql in project root

# Start Api

* `symfony serve`

# API (Accès à API Platform)

* `http://localhost/api`

## Liste des users

* `http://localhost/api/users` en GET sans aucun body.



# Docker
/!\ l'installation par docker est en maintenance
# Installation

* `docker-compose build`
* `docker-compose run php composer update`
* `docker-compose run php composer install`
* `docker-compose up -d`
* `docker-compose exec -T php php bin/console d:d:c` (General error: 1007 Can't create database 'wiredbeauty'; database exists)
* Delete all migrations if needed in `migration folder` and create with `docker-compose exec php php bin/console d:d:d --force` and `docker-compose exec php php bin/console make:migration` and `docker-compose exec php php bin/console d:m:m`

# Command line 

* Use `docker-compose exec -T php` before all command

# API (Accès à API Platform)

* `http://localhost/api`

## Liste des users

* `http://localhost/api/users` en GET sans aucun body.
