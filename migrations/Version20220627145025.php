<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220627145025 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE answer (id INT AUTO_INCREMENT NOT NULL, question_id INT NOT NULL, label VARCHAR(255) NOT NULL, value INT DEFAULT NULL, INDEX IDX_DADD4A251E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campaign (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, product_id INT NOT NULL, datestart DATE NOT NULL, dateend DATE NOT NULL, INDEX IDX_1F1512DD9395C3F3 (customer_id), INDEX IDX_1F1512DD4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, admin_id INT NOT NULL, title VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, picture VARCHAR(255) NOT NULL, INDEX IDX_5A8A6C8D642B8210 (admin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_D34A04AD9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, campaign_id INT NOT NULL, label VARCHAR(255) NOT NULL, INDEX IDX_B6F7494EF639F774 (campaign_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, campaign_id INT NOT NULL, datestart DATE NOT NULL, dateend DATE NOT NULL, isplacebo TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_D044D5D4F639F774 (campaign_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_tester (session_id INT NOT NULL, tester_id INT NOT NULL, INDEX IDX_933B5C1C613FECDF (session_id), INDEX IDX_933B5C1C979A21C1 (tester_id), PRIMARY KEY(session_id, tester_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tester (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, dateofbirth DATE NOT NULL, weight VARCHAR(255) NOT NULL, height VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tester_answer (id INT AUTO_INCREMENT NOT NULL, session_id INT NOT NULL, question_id INT NOT NULL, tester_id INT NOT NULL, answer_id INT NOT NULL, INDEX IDX_1BA97A46613FECDF (session_id), INDEX IDX_1BA97A461E27F6BF (question_id), INDEX IDX_1BA97A46979A21C1 (tester_id), INDEX IDX_1BA97A46AA334807 (answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A251E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_1F1512DD9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_1F1512DD4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D642B8210 FOREIGN KEY (admin_id) REFERENCES admin (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494EF639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4F639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE session_tester ADD CONSTRAINT FK_933B5C1C613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_tester ADD CONSTRAINT FK_933B5C1C979A21C1 FOREIGN KEY (tester_id) REFERENCES tester (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tester_answer ADD CONSTRAINT FK_1BA97A46613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE tester_answer ADD CONSTRAINT FK_1BA97A461E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE tester_answer ADD CONSTRAINT FK_1BA97A46979A21C1 FOREIGN KEY (tester_id) REFERENCES tester (id)');
        $this->addSql('ALTER TABLE tester_answer ADD CONSTRAINT FK_1BA97A46AA334807 FOREIGN KEY (answer_id) REFERENCES answer (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D642B8210');
        $this->addSql('ALTER TABLE tester_answer DROP FOREIGN KEY FK_1BA97A46AA334807');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494EF639F774');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D4F639F774');
        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DD9395C3F3');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD9395C3F3');
        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DD4584665A');
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A251E27F6BF');
        $this->addSql('ALTER TABLE tester_answer DROP FOREIGN KEY FK_1BA97A461E27F6BF');
        $this->addSql('ALTER TABLE session_tester DROP FOREIGN KEY FK_933B5C1C613FECDF');
        $this->addSql('ALTER TABLE tester_answer DROP FOREIGN KEY FK_1BA97A46613FECDF');
        $this->addSql('ALTER TABLE session_tester DROP FOREIGN KEY FK_933B5C1C979A21C1');
        $this->addSql('ALTER TABLE tester_answer DROP FOREIGN KEY FK_1BA97A46979A21C1');
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE session_tester');
        $this->addSql('DROP TABLE tester');
        $this->addSql('DROP TABLE tester_answer');
        $this->addSql('DROP TABLE user');
    }
}
