<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Campaign;
use App\Entity\Answer;
use Container7l8CACH\getCampaignRepositoryService;
use HTTP_Request2;
use Shuchkin\SimpleXLSX;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileUploader;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class UploadController extends AbstractController
{
    private $_entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->_entityManager = $entityManager;
    }


// ...

    /**
     * @Route("/test-upload", name="app_test_upload")
     */
    public function excelCommunesAction(Request $request, FileUploader $uploader)
    {
        //Get excel file
        $uploadedFile = $request->files->get('excelFile');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $filename = $uploadedFile->getClientOriginalName();
        $contain = $uploadedFile->getContent();
        $uploader->upload($uploadedFile);
        $rootDir = realpath($_SERVER["DOCUMENT_ROOT"]);
        ini_set("default_socket_timeout", 6000);

        $inputFileName = $rootDir . "/uploads/" . "questions.xlsx";
        $s = "";


        if ($xlsx = SimpleXLSX::parse($inputFileName)) {
            $tableau = $xlsx->rows();
        }
        $column = 0;

        //Get id campaign
        $id = (int)$request->request->get('idCampaign');
        //Get Campaign
        $cp = $this->_entityManager->getRepository(Campaign::class)->findOneBy(['id' => $id]);

        //loop first line of excel
        foreach ($tableau[0] as $montableau) {

            //In a question
            if (str_contains($montableau, "question")) {
                $question = new Question();

                $question->setLabel($tableau[1][$column]);
                $question->setCampaign($cp);


                $this->_entityManager->persist($question);
                $this->_entityManager->flush();

                $s .= $tableau[1][$column];
                $j = 0;

                //Answer with value
                if (str_contains($tableau[0][$column + 2], "value")) {
                    $row = 1;
                    while (isset($tableau[$row][$column + 1])) {
                        $rep = new Answer();
                        $rep->setlabel($tableau[$row][$column + 1]);
                        $rep->setQuestion($question);
                        $rep->setValue((int)$tableau[$row][$column + 2]);
                        $this->_entityManager->persist($rep);
                        $this->_entityManager->flush();
                        $row += 1;
                    }
                } else {
                    //Answer without value
                    $row = 1;
                    while (isset($tableau[$row][$column + 1])) {
                        // $s = $s + $tableau[$row][$column+1];
                        $rep = new Answer();
                        $rep->setlabel($tableau[$row][$column + 1]);
                        $rep->setQuestion($question);
                        $this->_entityManager->persist($rep);
                        $this->_entityManager->flush();
                        $row += 1;
                    }
                }
            }
            $column++;
        }
        dd("success");
    }

     /**
     * @Route("/uploadimage", name="upload_image")
     */
    public function uploadImage(Request $request, FileUploader $uploader)
    {
        //Get image file
        $uploadedFile = $request->files->get('image');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $filename = $uploadedFile->getClientOriginalName();
        $contain = $uploadedFile->getContent();
        $uploader->uploadImage($uploadedFile);
        $rootDir = realpath($_SERVER["DOCUMENT_ROOT"]);
        ini_set("default_socket_timeout", 6000);
        $nom = (string)$request->request->get('nom');

        $inputFileName = $rootDir . "\\uploads\\" . "image.png";
        copy($inputFileName, 'C:/Users/Utilisateur/Desktop/Cours/Projet_annuel/WiredBeauty_Api/wired-beauty-front/src/assets/'.$nom);
        dd("success");
    }
}