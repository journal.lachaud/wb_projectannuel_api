<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AnswerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"answer:read"}},
 *     denormalizationContext={"groups"={"answer:write"}}
 * )
 * @ORM\Entity(repositoryClass=AnswerRepository::class)
 */
class Answer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups({"answer:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"answer:read","answer:write","campaign:read"})
     *
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="answer")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"answer:read","answer:write"})
     *
     */
    private $question;

    /**
     * @ORM\OneToMany(targetEntity=TesterAnswer::class, mappedBy="answer")
     *
     * @Groups({"answer:read","answer:write"})
     *
     */
    private $testerAnswers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"answer:read","answer:write"})
     */
    private $value;

    public function __construct()
    {
        $this->testerAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection<int, TesterAnswer>
     */
    public function getTesterAnswers(): Collection
    {
        return $this->testerAnswers;
    }

    public function addTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if (!$this->testerAnswers->contains($testerAnswer)) {
            $this->testerAnswers[] = $testerAnswer;
            $testerAnswer->setAnswer($this);
        }

        return $this;
    }

    public function removeTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if ($this->testerAnswers->removeElement($testerAnswer)) {
            // set the owning side to null (unless already changed)
            if ($testerAnswer->getAnswer() === $this) {
                $testerAnswer->setAnswer(null);
            }
        }

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
