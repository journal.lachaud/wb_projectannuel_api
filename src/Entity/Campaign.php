<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"campaign:read"}},
 *     denormalizationContext={"groups"={"campaign:write"}}
 *     )
 * @ORM\Entity(repositoryClass=CampaignRepository::class)
 *
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups({"campaign:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     *
     * @Groups({"campaign:read","campaign:write"})
     */
    private $datestart;

    /**
     * @ORM\Column(type="date")
     *
     * @Groups({"campaign:read","campaign:write"})
     */
    private $dateend;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="campaigns")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"campaign:read"})
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="campaign", orphanRemoval=true)
     *
     *
     * @Groups({"campaign:read"})
     */
    private $questions;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="campaigns")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"campaign:read","campaign:write"})
     */
    private $product;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatestart(): ?\DateTimeInterface
    {
        return $this->datestart;
    }

    public function setDatestart(\DateTimeInterface $datestart): self
    {
        $this->datestart = $datestart;

        return $this;
    }

    public function getDateend(): ?\DateTimeInterface
    {
        return $this->dateend;
    }

    public function setDateend(\DateTimeInterface $dateend): self
    {
        $this->dateend = $dateend;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setCampaign($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getCampaign() === $this) {
                $question->setCampaign(null);
            }
        }

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
