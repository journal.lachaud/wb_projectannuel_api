<?php
//*  collectionOperations={
//    *          "multiple"={
//        *             "method"="POST",
// *             "path"="/questions/multiple",
// *             "input"=QuestionMultipleRequest::class,
// *             "output"=QuestionMultipleResponse::class,
// *             "controller"=CreateMultipleAction::class,
// *             "validation_groups"={"default", "question_multiple_create"},
// *             "openapi_context"={
//            *                 "summary"="Creates multiple questions resources.",
// *                 "description"="Creates at once multiple question entities, in a batch."
//                *             }
// *         }
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"question:read"}},
 *     denormalizationContext={"groups"={"question:write"}}
 *     )
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 *
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     *
     * @Groups({"question:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"question:read","question:write","campaign:read"})
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity=Campaign::class, inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"question:read"})
     */
    private $campaign;

    /**
     * @ORM\OneToMany(targetEntity=TesterAnswer::class, mappedBy="question", orphanRemoval=true)
     *
     * @Groups({"question:read"})
     */
    private $tester_answers;

    /**
     * @ORM\OneToMany(targetEntity=Answer::class, mappedBy="question", orphanRemoval=true)
     *
     * @Groups({"question:read","campaign:read"})
     */
    private $answer;

    public function __construct()
    {
        $this->tester_answers = new ArrayCollection();
        $this->answer = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Collection<int, TesterAnswer>
     */
    public function getTesterAnswers(): Collection
    {
        return $this->tester_answers;
    }

    public function addTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if (!$this->tester_answers->contains($testerAnswer)) {
            $this->tester_answers[] = $testerAnswer;
            $testerAnswer->setQuestion($this);
        }

        return $this;
    }

    public function removeTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if ($this->tester_answers->removeElement($testerAnswer)) {
            // set the owning side to null (unless already changed)
            if ($testerAnswer->getQuestion() === $this) {
                $testerAnswer->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Answer>
     */
    public function getAnswer(): Collection
    {
        return $this->answer;
    }

    public function addAnswer(Answer $answer): self
    {
        if (!$this->answer->contains($answer)) {
            $this->answer[] = $answer;
            $answer->setQuestion($this);
        }

        return $this;
    }

    public function removeAnswer(Answer $answer): self
    {
        if ($this->answer->removeElement($answer)) {
            // set the owning side to null (unless already changed)
            if ($answer->getQuestion() === $this) {
                $answer->setQuestion(null);
            }
        }

        return $this;
    }

}
