<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"session:read"}},
 *     denormalizationContext={"groups"={"session:write"}}
 *     )
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups({"session:read"})
     *
     *
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     *
     * @Groups({"session:read","session:write"})
     */
    private $datestart;

    /**
     * @ORM\Column(type="date")
     *
     * @Groups({"session:read","session:write"})
     */
    private $dateend;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"session:read","session:write"})
     */
    private $isplacebo;

    /**
     * @ORM\ManyToMany(targetEntity=Tester::class, inversedBy="sessions")
     *
     * @Groups({"session:read"})
     */
    private $testers;

    /**
     * @ORM\OneToOne(targetEntity=Campaign::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"session:read"})
     */
    private $campaign;

    /**
     * @ORM\OneToMany(targetEntity=TesterAnswer::class, mappedBy="session", orphanRemoval=true)
     *
     *
     * @Groups({"session:read"})
     */
    private $tester_answer;


    public function __construct()
    {
        $this->testers = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->tester_answer = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatestart(): ?\DateTimeInterface
    {
        return $this->datestart;
    }

    public function setDatestart(\DateTimeInterface $datestart): self
    {
        $this->datestart = $datestart;

        return $this;
    }

    public function getDateend(): ?\DateTimeInterface
    {
        return $this->dateend;
    }

    public function setDateend(\DateTimeInterface $dateend): self
    {
        $this->dateend = $dateend;

        return $this;
    }

    public function getIsplacebo(): ?bool
    {
        return $this->isplacebo;
    }

    public function setIsplacebo(bool $isplacebo): self
    {
        $this->isplacebo = $isplacebo;

        return $this;
    }

    /**
     * @return Collection<int, Tester>
     */
    public function getTesters(): Collection
    {
        return $this->testers;
    }

    public function addTester(Tester $tester): self
    {
        if (!$this->testers->contains($tester)) {
            $this->testers[] = $tester;
        }

        return $this;
    }

    public function removeTester(Tester $tester): self
    {
        $this->testers->removeElement($tester);

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Collection<int, TesterAnswer>
     */
    public function getTesterAnswer(): Collection
    {
        return $this->tester_answer;
    }

    public function addTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if (!$this->tester_answer->contains($testerAnswer)) {
            $this->tester_answer[] = $testerAnswer;
            $testerAnswer->setSession($this);
        }

        return $this;
    }

    public function removeTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if ($this->tester_answer->removeElement($testerAnswer)) {
            // set the owning side to null (unless already changed)
            if ($testerAnswer->getSession() === $this) {
                $testerAnswer->setSession(null);
            }
        }

        return $this;
    }
}
