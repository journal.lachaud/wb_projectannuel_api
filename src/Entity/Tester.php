<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TesterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"tester:read"}},
 *     denormalizationContext={"groups"={"tester:write"}}
 *     )
 * @ORM\Entity(repositoryClass=TesterRepository::class)
 */
class Tester
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     *
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $city;

    /**
     * @ORM\Column(type="date")
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $dateofbirth;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $weight;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $height;

    /**
     * @ORM\ManyToMany(targetEntity=Session::class, mappedBy="testers")
     *
     * @Groups({"tester:read"})
     */
    private $sessions;

    /**
     * @ORM\OneToMany(targetEntity=TesterAnswer::class, mappedBy="tester")
     *
     * @Groups({"tester:read"})
     */
    private $testerAnswers;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"tester:read","tester:write"})
     */
    private $password;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->testerAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDateofbirth(): ?\DateTimeInterface
    {
        return $this->dateofbirth;
    }

    public function setDateofbirth(\DateTimeInterface $dateofbirth): self
    {
        $this->dateofbirth = $dateofbirth;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return Collection<int, Session>
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->addTester($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            $session->removeTester($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, TesterAnswer>
     */
    public function getTesterAnswers(): Collection
    {
        return $this->testerAnswers;
    }

    public function addTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if (!$this->testerAnswers->contains($testerAnswer)) {
            $this->testerAnswers[] = $testerAnswer;
            $testerAnswer->setTester($this);
        }

        return $this;
    }

    public function removeTesterAnswer(TesterAnswer $testerAnswer): self
    {
        if ($this->testerAnswers->removeElement($testerAnswer)) {
            // set the owning side to null (unless already changed)
            if ($testerAnswer->getTester() === $this) {
                $testerAnswer->setTester(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
