<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TesterAnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"testeranswer:read"}},
 *     denormalizationContext={"groups"={"testeranswer:write"}})
 * @ORM\Entity(repositoryClass=TesterAnswerRepository::class)
 */
class TesterAnswer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups({"testeranswer:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Session::class, inversedBy="tester_answer")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"testeranswer:read"})
     */
    private $session;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="tester_answers")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"testeranswer:read"})
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity=Tester::class, inversedBy="testerAnswers")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"testeranswer:read"})
     */
    private $tester;

    /**
     * @ORM\ManyToOne(targetEntity=Answer::class, inversedBy="testerAnswers")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"testeranswer:read"})
     */
    private $answer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getTester(): ?Tester
    {
        return $this->tester;
    }

    public function setTester(?Tester $tester): self
    {
        $this->tester = $tester;

        return $this;
    }

    public function getAnswer(): ?Answer
    {
        return $this->answer;
    }

    public function setAnswer(?Answer $answer): self
    {
        $this->answer = $answer;

        return $this;
    }
}
